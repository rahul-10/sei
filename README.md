# sei



## Getting started
Run below commands - 
```
git clone git@gitlab.com:rahul-10/sei.git
cd sei
npm install
npm start
```

## Curl to check compliance against predefined policies
```
curl --location 'http://localhost:3000/check-compliance' \
--header 'Content-Type: application/json' \
--data '{
    "url": "https://www.joinguava.com/"
}'
```

Note: Policies are defined in policies.js file under configs folder
