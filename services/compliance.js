const puppeteer = require('puppeteer');
const { policies } = require('../configs/policies')

exports.checkCompliance = async (url) => {
    try {
        console.log('Process started...')
        const browser = await puppeteer.launch();
        const page = await browser.newPage();

        await page.goto(url, { waitUntil: 'domcontentloaded' });

        const nonCompliantResults = await page.evaluate((policies) => {
            console.log('policies: ', policies)
            const matchedPolicies = []
            for (let i = 0; i < policies.length; i++) {
                const policy = policies[i]
                if (document.body.innerText.toLowerCase().includes(policy.toLowerCase())) {
                    matchedPolicies.push(policy)
                }

            }
            return matchedPolicies
        }, policies);

        await browser.close();

        return nonCompliantResults;

    } catch (error) {
        console.log('error: ', error)
        throw error
    }
}