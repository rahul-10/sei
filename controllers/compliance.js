const { checkCompliance } = require('../services/compliance')

exports.checkComplianceController = async (req, res) => {
    const { url } = req.body
    try {
        const nonCompliantResults = await checkCompliance(url)
        if (nonCompliantResults?.length === 0) {
            return res.json({ compliant: true, matches: [] });
        } else {
            return res.json({ compliant: false, matches: nonCompliantResults });
        }
    } catch (error) {
        return res.status(500).json({ error: error.message || 'Internal Server Error' });
    }
}