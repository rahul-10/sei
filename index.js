const express = require('express');

const app = express();
const port = 3000; // we can also define in env variable

app.use(express.json());

process.on('uncaughtException', function (err) {
    console.error('uncaughtException: ', err)
})

const { checkComplianceController } = require('./controllers/compliance')

app.post('/check-compliance', checkComplianceController);

app.listen(port, () => {
    console.log(`Server is running on http://localhost:${port}`);
});
