exports.policies = [
    'Bank account',
    'Bank balance',
    'Banking',
    'Banking account',
    'Banking product',
    'Banking platform'
]